'use strict';

// Ответы на вопросы: 
// 1. Зачастую используется, когда необходимо работать с логикой запросов


const books = [
	{
		author: "Люсі Фолі",
		name: "Список запрошених",
		price: 70
	},
	{
		author: "Сюзанна Кларк",
		name: "Джонатан Стрейндж і м-р Норрелл",
	},
	{
		name: "Дизайн. Книга для недизайнерів.",
		price: 70
	},
	{
		author: "Алан Мур",
		name: "Неономікон",
		price: 70
	},
	{
		author: "Террі Пратчетт",
		name: "Рухомі картинки",
		price: 40
	},
	{
		author: "Анґус Гайленд",
		name: "Коти в мистецтві",
	}
];

const div = document.createElement("div");
const ul = document.createElement("ul");
div.appendChild(ul);

for (const book of books) {
	try {
		if (!book.author || !book.name || !book.price) {
			throw new Error(`В книге с именем '${book.name}' отсутствует одно или несколько свойств.`);
		}
		const li = document.createElement("li");
		li.textContent = `${book.author} - ${book.name} - ${book.price} грн`;
		ul.appendChild(li);
	} catch (error) {
		console.error(error);
	}
}





